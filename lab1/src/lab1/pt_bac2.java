package lab1;

import java.util.Scanner;

public class pt_bac2 {
public static void main(String[]args)
{
	int a,b,c;
	Scanner sc=new Scanner(System.in);
	System.out.println("phuong trinh bac hai: \n ax^2 + bx +c = 0\n nhap a=");
	a=sc.nextInt();
	System.out.println("nhap b=");
	b= sc.nextInt();
	System.out.println("nhap c=");
	c=sc.nextInt();
	sc.close();
	double delta=b*b-4*a*c;
	if(a==0)
	{
		if(b==0)
		{
			if(c==0)
				System.out.println("phuong trinh tren co vo so nghiem!");
			else
				System.out.println("phuong trinh tren vo nghiem!");
		}
		else
		{
			float x=-c/b;
			System.out.println("phuong trinh tren co nghiem la:" + x);
		}
	}
	else
	{	double x0=Math.sqrt(delta);
		double x1=(-b+x0)/(2*a);
		double x2=(-b-x0)/(2*a);
		float x3=-b/(2*a);
		if(delta>0)
			System.out.println("phuong trinh co hai nghiem la"+x1+" va "+x2);
		else if(delta==0)
			System.out.println("phuong trinh co mot nghiem duy nhat la: "+x3);
		else
			System.out.println("phuong trinh vo nghiem");
	}
}
}
