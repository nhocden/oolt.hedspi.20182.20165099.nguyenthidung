
public class Aims {

	public static void main(String[] args) {
		Order anOrder =new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		System.out.println(dvd1);
		anOrder.addDigitalVideoDisc(dvd1);
		System.out.println(anOrder.totalCost());

		DigitalVideoDisc dvd2 = new DigitalVideoDisc(dvd1);
		dvd2.setCategory("hello");
		// System.out.println(dvd2.getCategory());
		// System.out.println(dvd1.getCategory());
		// System.out.println(dvd1.getTitle());
		System.out.println(dvd2);
		anOrder.addDigitalVideoDisc(dvd2);
		System.out.println(anOrder.totalCost());
		
		DigitalVideoDisc dvd3= new DigitalVideoDisc("DUNG","HI","DENNHOC",99,30f);
		System.out.println(dvd3);		
		anOrder.addDigitalVideoDisc(dvd3);
		System.out.println(anOrder.totalCost());
		
		DigitalVideoDisc dvd4=dvd3;
		dvd4.setCategory("bye");
		System.out.println(dvd4);
		anOrder.addDigitalVideoDisc(dvd4);
		System.out.println(anOrder.totalCost());
		
//		System.out.println(anOrder.totalCost());
		DigitalVideoDisc dvd5=dvd3;
		anOrder.removeDigitalVideoDisc(dvd5);
		System.out.println(anOrder.totalCost());
	}
}
