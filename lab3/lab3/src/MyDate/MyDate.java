import java.util.Calendar;
import java.util.Scanner;
public class MyDate {
	
	private int day;
	private int month;
	private int year;
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void print() {
		System.out.println( "MyDate [day=" + day + ", month=" + month + ", year=" + year + "]");
	}
	public MyDate()
	{
		Calendar cal =Calendar.getInstance();
		day=cal.get(Calendar.DAY_OF_MONTH);
		month=cal.get(Calendar.MONTH)+1;
		year=cal.get(Calendar.YEAR);
	}
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String v)
	{
		setDate(v);
		
	}
	private void setDate(String v) {
		String word[]=v.split(" ");
		day=Integer.parseInt(word[1]);
		year=Integer.parseInt(word[2]);
		switch(word[0])
		{
			case "January" :month=1;break;
			case "February":month=2;break;
			case "March":month=3;break;
			case "April":month=4;break;
			case "May":month=5;break;
			case "June":month=6;break;
			case "July":month=7;break;
			case "August":month=8;break;
			case "September":month=9;break;
			case "October":month=10;break;
			case "November":month=11;break;
			case "December":month=12;break;
			default: System.out.printf("ngay thang khong hop le\n");break;
		}
	} 
	public void accept()
	{
		Scanner sr=new Scanner(System.in);
		String v;
		System.out.println("moi nhap ngay thang(chuoi:thang ngay nam)");
		v=sr.nextLine();
		setDate(v);
	}
	
}
