package hust.soict.ictglobal.aims.order;
import java.util.ArrayList;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
//import java.util.Arrays;

public class Order {
	
	public static final int MAX_NUMBERS_ORDERED=10;
	private ArrayList <Media> itemsOrdered = new ArrayList <Media> ();
	String dateOrdered;
	public static final int MAX_LIMITTED_ORDERS =5;
	private static int nbOrders =0;
	private int id;
	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}
	public ArrayList<Media> getItemsOrdered() {
		return itemsOrdered;
	}

	public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
		this.itemsOrdered = itemsOrdered;
	}

	public Order(String dateOrdered)
	{
		if(nbOrders<MAX_LIMITTED_ORDERS)
		{
			nbOrders++;
			setDateOrdered(dateOrdered);
		}
		else
			System.out.println("gioi han la 5 order cho moi nguoi");
	}
	public String getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public void addMedia(Media md) {
		itemsOrdered.add(md);
		
	}
	public void removeMedia(Media md) {
		itemsOrdered.remove(md);
	}
	public float totalCost() {
		float sum = 0;
    	for (int i = 0; i < itemsOrdered.size(); i++) {
    		sum=sum+itemsOrdered.get(i).getCost();
			
		}
    	return sum;
	}
//	public void addDigitalVideoDisc(DigitalVideoDisc v)
//	{
//		add(v);
//	}
//	private void add(DigitalVideoDisc v) {
//		if(qtyOrdered==10)
//			System.out.println("The order is almost full");
//		else
//	{
//		qtyOrdered++;
//		itemOrdered[qtyOrdered]=v;
//		System.out.println("The disc has been added and quantity is "+ qtyOrdered);
//	}
//	}
//	public void addDigitalVideoDisc(DigitalVideoDisc []v)
//	{
//		int m=v.length;
//		int i;
//		for(i=0;i<m;i++)
//		{
//			if(qtyOrdered==10)
//			{	System.out.println("the order is almost full");
//				break;
//			}
//			else
//			{
//				itemOrdered[qtyOrdered+1]=v[i];				
//			}
//		}
//		if(i<(m-1))
//		{
//			System.out.println("the order is almost full and the list of items that cannot be added");
//			for(int j=i;j<m;j++)
//				v[j].toString();
//		}
//		else
//			System.out.println("The disc has been added and quantity is "+ qtyOrdered);
//	}
//	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2)
//	{
//		add(dvd1);
//		if(qtyOrdered==10)
//			dvd1.toString();
//		add(dvd2);
//		if(qtyOrdered==10)
//			dvd1.toString();
//	}
//	public int sosanh(DigitalVideoDisc v1,DigitalVideoDisc v2)
//	{
//		if(v1.getTitle().equals(v2.getTitle()))
//			if(v1.getCategory().equals(v2.getCategory()))
//				if(v1.getDirector().equals(v2.getDirector()))
//					if(v1.getLength()==v2.getLength())
//						if(v1.getCost()==v2.getCost())
//							return 1;
//		return 0;
//	}
//
//	public void removeDigitalVideoDisc(DigitalVideoDisc v)
//	{
//	int x=qtyOrdered;
//	for(int i=1;i<=qtyOrdered;i++)
//	{
//		if(sosanh(itemOrdered[i],v)==1)
//		{
//			for(int j=i;j<qtyOrdered;j++)
//			{
//				itemOrdered[j]=itemOrdered[j+1];
//			}
//			qtyOrdered--;
//			
//		}
//	}
//	if(x==qtyOrdered)
//		System.out.println("the disc hasn't same that disc");
//	else
//		System.out.println("the disc has been remove");
//	}
//	public float totalCost()
//	{
//		float m=0;
//		for(int i=1;i<=qtyOrdered;i++)
//		{
//			m+=itemOrdered[i].getCost();
//		}
//		return m;
//	}
//	public void string()
//	{
//		System.out.println("***************Order**************");
//		System.out.println("Date:"+dateOrdered+"\nOrdered Items:");
//		for(int i=1;i<=qtyOrdered;i++)
//		{
//			System.out.println(i+".DVD-"+itemOrdered[i].getTitle()+"-"+itemOrdered[i].getCategory()+"-"+itemOrdered[i].getDirector()+"-"+itemOrdered[i].getLength()+":"+itemOrdered[i].getCost()+"$");			
//		}
//		System.out.println("Total cost:"+totalCost());
//		System.out.println("**********************************");
//		
//	}
//	public DigitalVideoDisc getALuckyItem(){
//		int a = (int)(Math.random() * this.qtyOrdered);
//		itemOrdered[a].setCost(0);
//		System.out.println("\n*****DVD duoc mien phi: " + itemOrdered[a].getTitle()+"*****");
//		return this.itemOrdered[a];
//	}
}

