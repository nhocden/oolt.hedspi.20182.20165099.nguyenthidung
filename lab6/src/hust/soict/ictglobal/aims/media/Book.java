package hust.soict.ictglobal.aims.media;
import java.util.ArrayList;
public class Book extends Media {
	private String title;
	private String category;
	private float cost;
	private ArrayList<String> authors =new ArrayList<String>();
	
	public Book() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<String> getAuthors() {
		return authors;
	}

	public void setAuthors(ArrayList<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName)
	{
		if(authors.contains(authorName)){
			System.out.print("The author is has exited");
		}
		else{
			authors.add(authorName);
		}
	}
	public void removeAuthor(String authorName)
	{
		if(authors.contains(authorName)){
			authors.remove(authorName);
		}
		else{
			System.out.println("The author is not exited\n");
		}
	}

	public Book(String title, String category, float cost, ArrayList<String> authors) {
		super(title,category,cost);
		this.authors = authors;
	}

	public Book(String title, String category, float cost) {
		super(title,category);
		this.cost = cost;
	}

	public Book(String title, String category) {
		super(title);
		this.category = category;
	}

	public Book(String title) {
		super();
		this.title = title;
	}
	
	
	
}
