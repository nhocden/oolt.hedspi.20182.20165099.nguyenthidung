package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Media {
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DigitalVideoDisc() {
		
	}
	public DigitalVideoDisc(String title) {
		super();
		this.title = title;
	}
	public DigitalVideoDisc(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}
	public DigitalVideoDisc(String title, String category, String director) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
	}
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	public DigitalVideoDisc(DigitalVideoDisc dvd1)
	{
		super();
		this.category=dvd1.getCategory();
		this.cost=dvd1.getCost();
		this.director=dvd1.getDirector();
		this.length=dvd1.getLength();
		this.title=dvd1.getTitle();
	}
	
	public void String1(){
		System.out.println( "DigitalVideoDisc [title=" + title + ", category=" + category + ", director=" + director + ", length="
				+ length + ", cost=" + cost + "]");
	}
	public boolean search(String title){
		String[] word = title.split(" ");
		int i;
		int j;
		System.out.println();
		for(j=0 ;j< word.length;j++){
			i = this.title.indexOf(word[j]);
			if(i<0){
				return false;
			}
		}
		return true;
	}
	
}
