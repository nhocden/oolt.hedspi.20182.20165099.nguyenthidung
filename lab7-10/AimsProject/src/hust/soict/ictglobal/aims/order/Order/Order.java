package hust.soict.hedspi.aims.order.Order;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
public class Order {
	public static final int MAX_NUMBERS_ORDERED=10;
	public static final int MAX_LIMITED_ORDERED=5;
	private static int nbOrders=0;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private Date dateOrdered;
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Order(){
		
	};
	public ArrayList<Media> getItemsOrdered() {
		return itemsOrdered;
	}

	public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
		this.itemsOrdered = itemsOrdered;
	}
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	public Order(String dateOrdered) {
		if(nbOrders<MAX_LIMITED_ORDERED) {
		try {
			this.dateOrdered = formatter.parse(dateOrdered);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		nbOrders ++;
		if(nbOrders >= MAX_LIMITED_ORDERED)
			System.out.println("full Order");
		}

	}
	
	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

//	public void printOrder() {
//		System.out.println("********Order******* " );
//		System.out.println("Date : " +getDateOrdered() );
//		System.out.println("Order items :" );
//		int i=0,n=getQtyOrdered();
//		for(i=0;i<=n;i++){
//			System.out.println("- DVD -" +itemsOrdered[i].getTitle()+"- " +itemsOrdered[i].getCategory()+"- " +itemsOrdered[i].getDirectory()+"- " +itemsOrdered[i].getLength()+"- " +itemsOrdered[i].getCost() );
//		}
//		
//		System.out.println("Total cost :" +totalCost());
//		System.out.println("************************* \n");
//		
//	}
	
	public float totalCost() {
		float sum = 0;
    	for (int i = 0; i < itemsOrdered.size(); i++) {
    		sum=sum+itemsOrdered.get(i).getCost();
			
		}
    	return sum;
		}
//	public DigitalVideoDisc getALuckyItem(){
//		int n=getQtyOrdered();
//		Random random = new Random();
//		int k= random.nextInt(n+1);
//		return itemsOrdered[k];
//	}
	public void addMedia(Media md) {
		itemsOrdered.add(md);
		
	}
	public void removeMedia(Media md) {
		itemsOrdered.remove(md);
	}
	
}
