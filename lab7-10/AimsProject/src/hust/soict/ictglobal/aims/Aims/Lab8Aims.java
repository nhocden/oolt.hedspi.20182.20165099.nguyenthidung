package hust.soict.hedspi.aims.Aims;

import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.media.Disc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Lab8Aims {
	public static void PlayAndSortDVD()throws PlayerException{
		Collection<DigitalVideoDisc> collection = new ArrayList<DigitalVideoDisc>();
		DigitalVideoDisc dvd1=new DigitalVideoDisc();
		dvd1.setTitle("F");
		dvd1.setLength(1);
		dvd1.setCost(18.2f);
		DigitalVideoDisc dvd2=new DigitalVideoDisc();
		dvd2.setTitle("D");
		dvd2.setLength(2);
		dvd2.setCost(30.6f);
		DigitalVideoDisc dvd3=new DigitalVideoDisc();
		dvd3.setTitle("C");
		dvd3.setLength(3);
		dvd3.setCost(14.9f);
		collection.add(dvd1);
		collection.add(dvd2);
		collection.add(dvd3);
		Iterator iterator = collection.iterator();
		while (iterator.hasNext()) {
			try {
				((DigitalVideoDisc)iterator.next()).play();
			} catch (PlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		iterator = collection.iterator();
		System.out.println("*******************************");
		System.out.println("The DVDs current in the order are:");
		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
			
		}
		Comparator<DigitalVideoDisc> comp = new Comparator<DigitalVideoDisc>() {
			public int compare(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2){
				return dvd1.compareTo(dvd2);
			}
		}; 
		Collections.sort((List<DigitalVideoDisc>)collection,comp);
		iterator = collection.iterator();
		System.out.println("*******************************");
		System.out.println("The DVDs in sorted by cost order are:");
		while (iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
			
		}
		System.out.println("*******************************");
	}
	
	
	public static void PlayAndSortCD() throws PlayerException {
		List<CompactDisc> discs = new ArrayList<CompactDisc>();
		CompactDisc cd1 = new CompactDisc("h�");
		cd1.addTrack( new Track("h�",10));
		cd1.addTrack( new Track("h�",4));
		cd1.addTrack( new Track("h�",1));
		CompactDisc cd2 = new CompactDisc("h�");
		cd2.addTrack( new Track("h�",2));
		cd2.addTrack( new Track("h�",1));
		cd2.addTrack( new Track("h�",13));
		CompactDisc cd3 = new CompactDisc("h�");
		cd3.addTrack( new Track("h�",6));
		cd3.addTrack( new Track("h�",4));
		cd3.addTrack( new Track("h�",9));
		discs.add(cd1);
		discs.add(cd2);
		discs.add(cd3);
		Iterator<CompactDisc> iterator = discs.iterator();
		while(iterator.hasNext()) {
			iterator.next().play();
			
		}
		iterator = discs.iterator();
		System.out.println("*******************************");
		System.out.println("The CDs currently in the order are:");
		while (iterator.hasNext()) {
			System.out.println(iterator.next().getTitle());
			
		}
		
		Comparator<CompactDisc> comp1 = new Comparator<CompactDisc>() {
			public int compare(CompactDisc cd1,CompactDisc cd2){
				return cd1.compareTo(cd2);
			}
		}; 
		
		Collections.sort(discs,comp1);
		iterator = discs.iterator();
		System.out.println("*******************************");
		System.out.println("The DVDs in sorted later order are:");
		while (iterator.hasNext()) {
			System.out.println((iterator.next()).getTitle());
			
		}
		System.out.println("*******************************");
		

	}
	



	public static void main(String[] args) throws PlayerException {
		System.out.println(".Play and sort a DVD list");
		PlayAndSortDVD();
		
		System.out.println(".Play and sort a CD list");
		PlayAndSortCD();
		// TODO Auto-generated method stub

	}

}
