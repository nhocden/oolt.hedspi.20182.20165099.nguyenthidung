package hust.soict.hedspi.aims.Aims;
import java.util.*;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order.Order;
import hust.soict.hedspi.aims.media.*;






public class Aims {
	private static int idOrder=1;
	public static void showMenu(){
		ArrayList<Order> listOrder = new ArrayList<Order>();
		Scanner input = new Scanner(System.in);
		int a=0;
		do{
		System.out.println("Order Management Application");
		System.out.println("------------------------------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("------------------------------------------------------");
		System.out.println("Please choose a number : 0-1-2-3-4");
		do{
			a=input.nextInt();
			if(a>=0&&a<5) break;
			System.out.println("Eror. Please input correctly");
			System.out.println("Please choose a number : 0-1-2-3-4");
			
			}while(true);
		switch(a){
		case 0:
			System.out.println("End Program !");
			break;
		case 1:
			createOrder(listOrder);
			break;
		case 2:
			addItem(listOrder);
			break;
		case 3:
			delItem(listOrder);
			break;
		case 4:
			showOrder(listOrder);
			break;
		default:
			break;
		}
		}while(a!=0);
	}
	public static boolean checkIdOrder(ArrayList<Order> listOrder,int id) {
		int a=0;
		for (int i = 0; i < listOrder.size(); i++) {
			if(listOrder.get(i).getId()==id) {
				a++;
				}
			
		}
		if(a==0)
			return false;		// không có Order trùng id
		else
			return true;		// có Order trùng id
	}
	public static void createOrder(ArrayList<Order> listOrder){
		Order order=new Order();
		System.out.println("Enter idOrder. If you don't have idOrder, press 0");
		Scanner input = new Scanner(System.in);
		int id=input.nextInt();
		if(id==0) {
			order.setId(idOrder);
			System.out.println("Your idOrder is "+idOrder);
			System.out.println("Please remember idOrder !!!!");
			idOrder++;
			listOrder.add(order);
		}else if(checkIdOrder(listOrder, id))
			System.out.println("Your Order ready");
		else System.out.println("Order has not already");
	}
	public static void addItem(ArrayList<Order> listOrder) {
		System.out.println("Enter idOrder");
		Scanner input = new Scanner(System.in);
		int id=input.nextInt();
		int check=0;
	
		
		for (int i = 0; i < listOrder.size(); i++) {
			if(listOrder.get(i).getId()==id) {
				check++;
				System.out.println("Book(1) or DVD(2) or CD(3) ??");
				
				
				int choose;
				choose = input.nextInt(); 
				switch(choose) {
				case 1:
					addBook(listOrder.get(i));
					break;
				case 2 :
					addDVD(listOrder.get(i));
					break;
				case 3:
					addCD(listOrder.get(i));
					break;
				default: break;
				}
			}
			
		}
		if(check==0)
			System.out.println("Order is not exited");
	}
	public static boolean checkIdItem(Order order, int id) {
		int check=0;
		for (int i = 0; i < order.getItemsOrdered().size(); i++) {
			if(order.getItemsOrdered().get(i).getId()==id) {
				check++;
			}
			
		}
		if(check>0) {
			System.out.println("Item is exited. Please enter again");
			return false;
		}
		else {
		System.out.println("Correct !");
		return true;
		}
		
	}
	public static void addBook(Order order){
		Book book=new Book();
		System.out.println("Enter Book's ID");
		Scanner input = new Scanner(System.in);
		int id = input.nextInt();
		while(checkIdItem(order,id)==false) {
			 id= input.nextInt();
		}
		book.setId(id);
		System.out.println("Enter title ");
		input.nextLine();                                 
		book.setTitle(input.nextLine());
		System.out.println("Enter category ");
		book.setCategory(input.nextLine());
		System.out.println("Enter cost ");
		book.setCost(Float.parseFloat(input.nextLine()));
		System.out.println("Enter authors separated by / ");
		String[] word = input.nextLine().split("/");
		
		List<String> authors = new ArrayList<String>();
		for (int i = 0; i < word.length; i++) {
			authors.add(word[i]) ;
		}
		book.setAuthors(authors);
		order.addMedia(book);
	}
	public static void addDVD(Order order){
		DigitalVideoDisc dvd=new DigitalVideoDisc();
		System.out.println("Enter DVD's ID");
		Scanner input = new Scanner(System.in);
		int id = input.nextInt();
		while(checkIdItem(order,id)==false) {
			 id= input.nextInt();
		}
		dvd.setId(id);
		System.out.println("Enter title ");
		input.nextLine();                                 
		dvd.setTitle(input.nextLine());
		System.out.println("Enter category ");
		dvd.setCategory(input.nextLine());
		System.out.println("Enter cost ");
		dvd.setCost(Float.parseFloat(input.nextLine()));
		System.out.println("Enter directory ");
		dvd.setCategory(input.nextLine());
		System.out.println("Enter length ");
		dvd.setLength(Integer.parseInt(input.nextLine()));
		
		order.addMedia(dvd);
		
	}
	public static void addCD(Order order){
		int numTrack=0;
		CompactDisc cd = new CompactDisc();
		System.out.println("Enter CD's ID");
		Scanner input = new Scanner(System.in);
		int idCd = input.nextInt();
		while(checkIdItem(order,idCd)==false) {
			 idCd= input.nextInt();
		}
		cd.setId(idCd);

		System.out.println("Enter title of CD :");
		input.nextLine();   
		cd.setTitle(input.nextLine());
		System.out.println("Enter artist ");
		cd.setArtist(input.nextLine());
		System.out.println("Enter number of track ");
		numTrack=input.nextInt();
		for (int i = 0; i < numTrack; i++) {
			Track newTrack = new Track();
			System.out.println("Information of Track" +(i+1));
			System.out.println("Enter track's title");
			input.nextLine();                
			newTrack.setTitle(input.nextLine());
			System.out.println("Enter track's length");
			newTrack.setLength(Integer.parseInt(input.nextLine()));
			cd.addTrack(newTrack);
		}
		order.addMedia(cd);
		System.out.println("Complete CD information");
		System.out.println("Can you turn on CD ?");
		System.out.println("Yes(1) or No(2) ");
		
		int chose = input.nextInt();
		if (chose==1) {
			System.out.println("CD is played on");
			try {
				cd.play();
			} catch (PlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				e.getMessage();
			}
		}
		else System.out.println("Thank you");
	}
	public static void delItem(ArrayList<Order> listOrder){
		System.out.println("Enter id Order");
		Scanner input = new Scanner(System.in);
		int idOrder=input.nextInt();
		int check=0;
		for (int i = 0; i < listOrder.size(); i++) {
			if(listOrder.get(i).getId()==idOrder) {
				check++;
				Order order = listOrder.get(i);
				System.out.println("Enter idItem");
				int idItem = input.nextInt();
				int check1=0;
				for (int j = 0; j < order.getItemsOrdered().size() ; j++) {
					if	(order.getItemsOrdered().get(j).getId()==idItem) {
						order.getItemsOrdered().remove(j);
						System.out.println("Item has removed");
						check1++;
					}
					
				}
				if(check1==0)
					System.out.println("Item not found");
				
				
				}
			
			}
		if(check==0)
			System.out.println("Order not found");
				
		
	}
	public static void showOrder(ArrayList<Order> listOrder) {
		System.out.println("Enter idOrder");
		Scanner input = new Scanner(System.in);
		int idOrder=input.nextInt();
		
		for (int i = 0; i < listOrder.size(); i++) {
			if(listOrder.get(i).getId()==idOrder) {
				Order order = listOrder.get(i);
				for (int j = 0; j < order.getItemsOrdered().size(); j++) {
					System.out.println(order.getItemsOrdered().get(j).toString());
					
				}
				System.out.println("Total Cost is: "+order.totalCost());
			
				
				}
		}
			
	}
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method st 
	Thread dt = new Thread(new MemoryDaemon(), "My Deamon Thread " );
	dt.setDaemon(true);
	dt.start();
	showMenu();
	Thread.sleep(5000);
	System.out.println("----------Finish--------------");
	
	}
}
