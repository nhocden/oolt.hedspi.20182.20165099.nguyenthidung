package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.Aims.PlayerException;




public class DigitalVideoDisc extends Disc implements Playable, Comparable{
	private String directory;
	private int length;
	
	
	public int compareTo(Object obj) {
		if (this.getCost()>((DigitalVideoDisc) obj).getCost())
			return 1;
		else if(this.getCost()<((DigitalVideoDisc) obj).getCost())
			return -1;
		else
			return 0;
		
	}
	public DigitalVideoDisc() {
		super();
	}


	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
	}


	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}


	public DigitalVideoDisc(String title) {
		super(title);
	}


	public DigitalVideoDisc(String title, String category, String directory,
			int length,float cost) {
		super(title, category,cost);
		this.directory = directory;
		this.length = length;
	}
	public DigitalVideoDisc(String title, String category, String directory){
		super(title,category);
		this.directory=directory;
	}


	public String toString() {
		return "DigitalVideoDisc [title=" + title + ", category=" + category
				+ ", directory=" + directory + ", length=" + length + ", cost="
				+ cost + "]";
	}


	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean search(String title){
		String a = title.toLowerCase();
		String [] word1 =a.split("\\s");
		String b = getTitle().toLowerCase();
		String [] word2 =b.split("\\s");
		int count=0;
		for(int i=0;i<word1.length;i++) { 
			for(int j=0;j<word2.length;j++) {
				if(word1[i].equals(word2[j])) {
					count++;
					word2[j]=null;
					break;
				}
			}
		}
		if (count==word1.length)
			return true;
			else
			return false;
	}
	@Override
	public void play() throws PlayerException {
		if(this.getLength()<=0){
			System.err.print("Error : DVD length is 0 ");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
		}
}
	
