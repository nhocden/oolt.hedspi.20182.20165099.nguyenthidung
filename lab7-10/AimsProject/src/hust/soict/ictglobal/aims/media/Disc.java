package hust.soict.hedspi.aims.media;

public class Disc extends Media {
	protected int length;
	protected String directory;
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String director) {
		this.directory = director;
	}
	public Disc(String title, String category, int length, String directory) {
		super(title, category);
		this.length = length;
		this.directory = directory;
	}
	public Disc() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Disc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}
	public Disc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}
	public Disc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
}
