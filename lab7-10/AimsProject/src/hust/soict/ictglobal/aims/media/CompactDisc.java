package hust.soict.hedspi.aims.media;





import hust.soict.hedspi.aims.Aims.PlayerException;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable {
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>(); 
	
//	public int compareTo(Object obj) {
//	return this.getTitle().compareTo(((CompactDisc) obj).getTitle());
//	}
	public int compareTo(Object obj) {
		if (this.tracks.size()>((CompactDisc) obj).tracks.size())
			return 1;
		else if(this.tracks.size()<((CompactDisc) obj).tracks.size())
			return -1;
		else 
		{
			if (this.getLength() > ((CompactDisc) obj).getLength())
				return 1;
			else if(this.getLength() < ((CompactDisc) obj).getLength())
				return -1;
			else
				return 0;
			
		}
			
		
	}
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public CompactDisc(String title, String category, int length,
			String directory, String artist) {
		super(title, category, length, directory);
		this.artist = artist;
	}

	public CompactDisc() {
	}
	public void addTrack(Track track ) {
		if(this.tracks.contains(track))
			System.out.println("track " + track.getTitle() + " da ton tai");
		else 
		{
			this.tracks.add(track);
			System.out.println("track " + track.getTitle() + " da duoc them ");
		}
			
		
	}
	
	public CompactDisc(String title, String artist) {
		super(title);
		this.artist = artist;
	}
	public CompactDisc(String title) {
		super(title);
	}

	public void removeTrack(Track track) {
		if(this.tracks.contains(track)) 
		{
			this.tracks.remove(track);
			System.out.println(" track "+ track.getTitle() + " da duoc xoa " );
		}
			
		else
			System.out.println(" track "+ track.getTitle() + " khong ton tai " );
	}
	public int getLength() {
		int lengthOfCD=0;
		for (int i = 0; i < this.tracks.size(); i++) 
		{
			lengthOfCD += tracks.get(i).getLength();
			
		}
		return lengthOfCD;
		
		
	}
	@Override
	public String toString() {
		return "CompactDisc [artist=" + artist + ", length=" + length
				+ ", tracks=" + tracks + ", directory=" + directory
				+ ", title=" + title + ", category=" + category + ", cost="
				+ cost + "]";
	}
	@Override
	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERROR: CD length is 0");
			throw(new PlayerException());
		}
		
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("Cd length: " + this.getLength());
		
		java.util.Iterator iter = tracks.iterator();
		Track nextTrack;
		
		while (iter.hasNext()) {
			nextTrack = (Track) iter.next();
			try {
				nextTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
	}

//	public void play(){
//		
//			System.out.println(" Play CD!  ");
//			System.out.println("Title :" + this.getTitle());
//			System.out.println("Length of CD :" + this.getLength());
//			System.out.println("Artis :"+this.getArtist());
//			for (int i = 0; i < this.tracks.size(); i++) {
//				System.out.println("Track "+(i+1));
//				tracks.get(i).play();
//				
//			}
		
	
}
