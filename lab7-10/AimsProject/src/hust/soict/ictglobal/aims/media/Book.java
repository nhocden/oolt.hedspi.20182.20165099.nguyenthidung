package hust.soict.hedspi.aims.media;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable {
	
	private String content;
	List<String> contentTokens=new ArrayList();
	Map<String,Integer> wordFrequency = new HashMap<String,Integer>() ;

	private List<String> authors = new ArrayList<String>();
	
	public Book() {
		// TODO Auto-generated constructor stub
	}
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
	public Book(String title, String category) {
		super(title, category);
	}
	public Book(String title) {
		super(title);
	}
	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	public List<String> getAuthors() {
		return authors;
	}
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public void addAuthor(String authorName){
		boolean y;
		int x= authors.indexOf(authorName);
		if(x<0){
			y = authors.add(authorName);
			/*if(y==true)
				System.out.print("Ten tac gia da duoc them vao!");
			else
				System.out.print("Ten tac gia chua duoc them vao!");*/
		}
		else
			System.out.println("Ten tac gia da ton tai!");
	}
	
	public void removeAuthor(String authorName){
		boolean y;
		int x= authors.indexOf(authorName);
		if(x>0){
			y = authors.add(authorName);
			if(y==true)
				System.out.print("Ten tac gia da duoc xoa!");
			else
				System.out.print("Ten tac gia chua duoc xoa!");
		}
		else
			System.out.println("Ten tac gia khong ton tai!");
	}
	public String toString(int id){
		System.out.println(this.content);
		System.out.println(this.wordFrequency);
		return "Completed";
	}
	public String toString() {
		return "Book [authors=" + authors + ", title=" + title + ", category="
				+ category + ", cost=" + cost + ", id=" + id + "]";
	}
	public int compareTo(Object obj) {
		if (this.getTitle() == ((Book)obj).getTitle())
			return 0;
		else if (this.getTitle().compareTo(((Book)obj).getTitle()) >0 )
			return 1;
		else
			return -1;
	}
	
	public void processContent() {
		String [] tokenArray = this.content.split("\\.|\\s|\\,");
		for (String word : tokenArray) {
			contentTokens.add(word);
			
		}
		
		Comparator<String> comp = new Comparator<String>() {
			public int compare(String token1, String token2) {
				return token1.compareTo(token2);
			}
		};
		
		Collections.sort(contentTokens,comp);
		
		
		
		wordFrequency = new LinkedHashMap<String,Integer>();
		
		for (int i = 0; i < contentTokens.size()-1; i++) {
			String sameWord = contentTokens.get(i);
			int count = 1;
			while(sameWord.equals(contentTokens.get(i+1))){
				count++;
				i++;
				
			}
			
			wordFrequency.put(sameWord, count);
			
		}
		
		
		
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
