package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.Aims.PlayerException;

public interface Playable {
	public void play() throws PlayerException;
}
