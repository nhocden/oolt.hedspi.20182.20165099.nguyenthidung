package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.Aims.PlayerException;

import javax.swing.text.PlainView;



public class Track implements Playable, Comparable {
	private String title;
	private int length;
	
	public int compareTo(Object obj) {
		return this.getTitle().compareTo(((Track) obj).getTitle());
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	public void play() throws PlayerException{
		if(this.getLength()<=0){
			System.err.print("\nERR: The track leng is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
		}
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}

	public Track(String title) {
		super();
		this.title = title;
	}

	public Track() {
		// TODO Auto-generated constructor stub
	}

}
