package hust.soict.hedspi.Lab10Exercise;

import java.util.Scanner;

public class Student {
	private int studentID;
	private String studentName;
	private String birthday;
	private float gpa;
	public int getStudentID() {
		return studentID;
	}
	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public float getGpa() {
		return gpa;
	}
	public void setGpa(float gpa) {
		this.gpa = gpa;
	}
	public Student(){
		
	}
	public Student(int studentID, String studentName, String birthday, float gpa) {
		super();
		this.studentID = studentID;
		this.studentName = studentName;
		this.birthday = birthday;
		this.gpa = gpa;
	}
	public void manager(){
		Scanner input = new Scanner(System.in);
		System.out.println("-------STUDENT INFORMATION-----");
		System.out.println("Enter ID: ");
		this.setStudentID(Integer.parseInt(input.nextLine()));
		System.out.println("Enter name: ");
		this.setStudentName(input.nextLine());
		try{
			System.out.println("Enter Date of birthday: ");
			String date = input.nextLine();
			checkDate(date);
			this.setBirthday(date);
		}catch(IllegalBirthDayException e){
			e.printStackTrace();
		}
		System.out.println("Enter GPA: ");
		try{
			float Gpa = Float.parseFloat(input.nextLine());
			checkGpa(Gpa);
			this.setGpa(Gpa);
		}catch(IllegalGPAException e){
			e.printStackTrace();
		}
		
		
		
	}
	private void checkGpa(float gpa2) throws IllegalGPAException {
		if(0<=gpa2&&gpa<=4){
			System.out.print("\n Complete !");
		}
		else throw(new IllegalGPAException());
	}
	public void checkDate(String date) throws IllegalBirthDayException{
		String[] word = date.split("/");
		int d = Integer.parseInt(word[0]);
		int m = Integer.parseInt(word[1]);
		int y = Integer.parseInt(word[2]);
		if(check(d,m,y)==0){
			throw (new IllegalBirthDayException());
		}
	}
	public  int check(int d,int m,int y){
		int check=0;
		if(1000<=y&&y<=9999){
			switch (m) {
			case 1:
				if(1<=d&&d<=31) check=1;
				break;
			case 2:
				if(1<=d&&d<=29) check=1;
				break;
			case 3:
				if(1<=d&&d<=31) check=1;
				break;
			case 4:
				if(1<=d&&d<=30) check=1;
				break;
			case 5:
				if(1<=d&&d<=31) check=1;
				break;
			case 6:
				if(1<=d&&d<=30) check=1;
				break;
			case 7:
				if(1<=d&&d<=31) check=1;
				break;
			case 8:
				if(1<=d&&d<=31) check=1;
				break;
			case 9:
				if(1<=d&&d<=30) check=1;
				break;
			case 10:
				if(1<=d&&d<=31) check=1;
				break;
			case 11:
				if(1<=d&&d<=30) check=1;
				break;
			case 12:
				if(1<=d&&d<=31) check=1;
				break;
			default:
				break;
			}
				
		}
		return check;
		
	}
	@Override
	public String toString() {
		return "Student [studentID=" + studentID + ", studentName="
				+ studentName + ", birthday=" + birthday + ", gpa=" + gpa + "]";
	}
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.manager();
		s1.toString();
		
	}
	
}
