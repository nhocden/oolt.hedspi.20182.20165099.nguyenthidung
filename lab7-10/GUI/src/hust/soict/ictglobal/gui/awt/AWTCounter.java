package hust.soict.hedspi.gui.awt;

import java.awt.*;
import java.awt.event.*;
public class AWTCounter extends Frame implements ActionListener {
	
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;
	
	public AWTCounter () {
		setLayout(new FlowLayout());
		lblCount = new Label("Counter");
		add(lblCount);
		tfCount = new TextField(count + "", 10);
		tfCount.setEditable(false);
		add(tfCount);
		add(lblCount);
// "super" Frame container adds Label component
		tfCount = new TextField(count + "", 10);
// construct the TextField component with initial text
		tfCount.setEditable(false);
// set to read-only
		add(tfCount);
// "super" Frame container adds TextField component
		btnCount = new Button("Count");
// construct the Button component
		add(btnCount);
// "super" Frame container adds Button component
		btnCount.addActionListener(this);
// "btnCount" is the source object that fires an ActionEvent when clicked.
// The source add "this" instance as an ActionEvent listener, which provides
// an ActionEvent handler called actionPerformed().
// Clicking "btnCount" invokes actionPerformed().
		setTitle("AWT Counter");
// "super" Frame sets its title
		setSize(250, 100);
// "super" Frame sets its initial window size
// For inspecting the Container/Components objects
// System.out.println(this);
// System.out.println(lblCount);
// System.out.println(tfCount);
// System.out.println(btnCount);
		setVisible(true);
// "super" Frame shows
// System.out.println(this);
// System.out.println(lblCount);
// System.out.println(tfCount);
// System.out.println(btnCount);
	}
// The entry main() method
public static void main(String[] args) {
// Invoke the constructor to setup the GUI, by allocating an instance
	AWTCounter app = new AWTCounter();
// or simply "new AWTCounter();" for an anonymous instance
	}
// ActionEvent handler-Called back upon button-click.
@Override
public void actionPerformed(ActionEvent evt) {
++count;
// Increase the counter value
// Display the counter value on the TextField tfCount
tfCount.setText(count + "");
// Convert int to String
	}
	

}
